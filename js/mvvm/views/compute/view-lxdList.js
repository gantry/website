// ============================================
// LxdList View
// ============================================

views.LxdList = Backbone.View.extend({

    tagName: 'tbody',

    initialize: function(options) {
        // Ensure our methods keep the `this` reference to the view itself
        _.bindAll(this, 'render');

        // If the model changes we need to re-render
        this.model.bind('change', this.render);

        // Not all views are self-rendering. This one is.
        this.render();
    },

    render: function() {

        // Check Inputs (Debug Info)
        console.log("LxdList render : ");

        // Clear
        jQuery(this.el).empty();
        jQuery("#lxdList").empty();
        jQuery("#playbookList").empty();


        // Add Table Header
        jQuery("#lxdList").append(jQuery(
            '<thead>'
            + '<tr>'
            + '<th class="tabblue serviceHeader">Name</th>'
            + '<th class="tabblue serviceHeader">Status</th>'
            + '<th class="tabblue serviceHeader">Ip Address</th>'
            + '<th class="tabblue serviceHeader">Controls</th>'
            + '</tr>'
            + '</thead>'
        ));

        // LXD Compute List
        lxdList = this.model.get('containers_all')
        console.log("LxdList");
        console.log(lxdList);

        // For each Container
        for (var i = 0; i < lxdList.length; i++)
        {
            containerList= lxdList[i].containers;
            for (var j = 0; j < containerList.length; j++)
            {
                container = containerList[j];
                console.log(container.name);

                jQuery(this.el).append(jQuery(
                    '<tr class="serviceItem">'
                    + '<td>'+ container["name"] + '</td>'
                    + '<td>'+ container["settings"].flags  + '</td>'
                    + '<td><a href="http://'+ container["settings"].ipv4  + '"><div class="serviceLink">'+ container["settings"].ipv4  + '</div></a></td>'
                    + '<td>'
                    + '<span><i class="fa fa-play fa-2x buttonSpacer" aria-hidden="true"></i></span>'
                    + '<span><i class="fa fa-stop fa-2x buttonSpacer" aria-hidden="true"></i></span>'
                    + '<span><i class="fa fa-trash fa-2x buttonSpacer" aria-hidden="true"></i></span>'
                    + '</tr>'
                ));


            }
            lxd = lxdList[i];
            console.log(lxd);
        }


        // Add Table Header
        jQuery("#playbookList").append(jQuery(
            '<thead>'
            + '<tr>'
            + '<th class="tabblue serviceHeader">Playbooks</th>'
            + '</tr>'
            + '</thead>'
        ));

        // Playbook List
        playbookList = this.model.get('playbooks')
        console.log("LxdList");
        console.log(playbookList);
        for (var i = 0; i < playbookList.length; i++)
        {
            playbook = playbookList[i];
            console.log(playbook);
            jQuery("#playbookList").append(jQuery(
                '<tr class="serviceItem">'
                + '<td>'+ playbook + '</td>'
                + '</tr>'
            ));
        }


//        jQuery("#playbookList").append(jQuery(
//            '<div>'
//            + '<script>$(document).foundation();</script>'
//            + '</div>'
//        ));

        return this;
    }
});


views.LxdListView = Backbone.View.extend({

    // The collection will be kept here
    collection: null,
    el: '#lxdList',

    initialize: function(options){
        this.collection = options.collection;

        // Ensure our methods keep the `this` reference to the view itself
        _.bindAll(this, 'render');

        // Bind collection changes to re-rendering
        this.collection.bind('reset', this.render);
        this.collection.bind('add', this.render);
        this.collection.bind('remove', this.render);

    },
    template: _.template("<h3>Hello</h3><% _.each(collection, function(model) { %><%= model %><% }); %>"),

    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){


        //alert ("Collection: " + this.collection);
        var element = jQuery(this.el);
        // Clear potential old entries first
        element.empty();

        console.log("LxdList");
        console.log(this.collection);

        // Go through the collection items
        this.collection.forEach(function(item) {
            //alert("here: " + item);

            var itemView = new views.LxdList({
                model: item
            });
            console.log("LxdListItem");
            console.log(item);

            // Render the View
            element.append(itemView.render().el);
        });

        //this.$el.html(this.template({
        //	who: "Fred",
        //  collection: this.collection
        //}));

        return this;
    }
});

