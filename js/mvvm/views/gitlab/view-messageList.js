// ============================================
// MessageList View
// ============================================

views.MessageList = Backbone.View.extend({

    tagName: 'tbody',

    initialize: function(options) {
        // Ensure our methods keep the `this` reference to the view itself
        _.bindAll(this, 'render');

        // If the model changes we need to re-render
        this.model.bind('change', this.render);

        // Not all views are self-rendering. This one is.
        this.render();
    },

    render: function() {

        // Check Inputs (Debug Info)
        console.log("MessageList render : ");

        // Clear
        jQuery(this.el).empty();
        jQuery("#messageList").empty();

        // Add Table Header
        jQuery("#messageList").append(jQuery(
            '<thead>'
            + '<tr>'
            + '<th class="tabblue">Message</th>'
            + '<th class="tabblue">Id</th>'
            + '<th class="tabblue">Active</th>'
            + '<th class="tabblue">Start</th>'
            + '<th class="tabblue">Stop</th>'
            + '</tr>'
            + '</thead>'
        ));

        messageList = this.model.get('broadcastmessages').messages;
        console.log("MessageList");
        console.log(messageList);
        for (var i = 0; i < messageList.length; i++)
        {
            message = messageList[i];
            console.log(message);

            jQuery(this.el).append(jQuery(
                '<tr class="serviceItem"'
                + 'style="background-color:'+ message["color"] + ';color:' + message["font"] + '">'
                + '<td>'+ message["message"] + '</td>'
                + '<td>'+ message["id"]  + '</td>'
                + '<td>'+ message["active"]  + '</td>'
                + '<td>'+ message["starts_at"]  + '</td>'
                + '<td>'+ message["ends_at"]  + '</td>'
//                + '<td>'+ message["color"]  + '</td>'
//                + '<td>'+ message["font"]  + '</td>'

                + '</tr>'
            ));

//            jQuery(this.el).append(jQuery(
//                '<div class="serviceItem"'
//                + 'style="background-color:'+ message["color"] + ';color:' + message["font"] + '">'
//                + '<hr/>'
//                + '<b>'+ message["message"] + '</b>'
//                + '<ul>'
//
//                + '<li>'+ message["id"]  + '</li>'
//                + '<li>'+ message["active"]  + '</li>'
//                + '<li>'+ message["starts_at"]  + '</li>'
//                + '<li>'+ message["ends_at"]  + '</li>'
////                + '<li>'+ message["color"]  + '</li>'
////                + '<li>'+ message["font"]  + '</li>'
//
//                + '</ul>'
//                + '</div>'
//            ));

        }

        return this;
    }
});


views.MessageListView = Backbone.View.extend({

    // The collection will be kept here
    collection: null,
    el: '#messageList',

    initialize: function(options){
        this.collection = options.collection;

        // Ensure our methods keep the `this` reference to the view itself
        _.bindAll(this, 'render');

        // Bind collection changes to re-rendering
        this.collection.bind('reset', this.render);
        this.collection.bind('add', this.render);
        this.collection.bind('remove', this.render);

    },
    template: _.template("<h3>Hello</h3><% _.each(collection, function(model) { %><%= model %><% }); %>"),

    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){


        //alert ("Collection: " + this.collection);
        var element = jQuery(this.el);
        // Clear potential old entries first
        element.empty();

        console.log("MessageList");
        console.log(this.collection);

        // Go through the collection items
        this.collection.forEach(function(item) {
            //alert("here: " + item);

            var itemView = new views.MessageList({
                model: item
            });
            console.log("MessageListItem");
            console.log(item);

            // Render the View
            element.append(itemView.render().el);
        });

        //this.$el.html(this.template({
        //	who: "Fred",
        //  collection: this.collection
        //}));

        return this;
    }
});

