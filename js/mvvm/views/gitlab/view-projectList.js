// ============================================
// ProjectList View
// ============================================

views.ProjectList = Backbone.View.extend({

    tagName: 'div',

    initialize: function(options) {
        // Ensure our methods keep the `this` reference to the view itself
        _.bindAll(this, 'render');

        // If the model changes we need to re-render
        this.model.bind('change', this.render);

        // Not all views are self-rendering. This one is.
        this.render();
    },

    render: function() {

        // Check Inputs (Debug Info)
        console.log("Projectlist render : ");
        console.log(this.model.get('success').success );
        console.log(this.model.get('attributes').name );

        // Clear
        jQuery(this.el).empty();

//        // Success
//        jQuery(this.el).append(jQuery(
//            '<div class="">'
//            + '<p>'+ this.model.get('success').success + '</p>'
//            //+ this.model.get('_keywords')
//            + '</div>'
//
//        ));


        // Attributes
        attributesList = this.model.get('attributes');
        jQuery(this.el).append(jQuery(
            '<div class="serviceItem">'
            + '<hr/>'
            + '<b>'+ attributesList.name + '</b>'
            + '<ul>'

            + '<li>'+ attributesList.name  + '</li>'
            + '<li>'+ attributesList.web_url  + '</li>'
            + '<li>'+ attributesList.ssh_url_to_repo  + '</li>'
            + '<li>'+ attributesList.readme_url  + '</li>'
            + '<li>'+ attributesList.visibility  + '</li>'
            + '<li>'+ attributesList.path_with_namespace  + '</li>'
            + '<li>'+ attributesList.http_url_to_repo  + '</li>'
            + '<li>'+ attributesList.last_activity_at  + '</li>'
            + '<li>'+ attributesList.created_at  + '</li>'

            + '</ul>'
            + '</div>'
        ));


        return this;
    }
});


views.ProjectListView = Backbone.View.extend({

    // The collection will be kept here
    collection: null,
    el: '#projectList',

    initialize: function(options){
        this.collection = options.collection;

        // Ensure our methods keep the `this` reference to the view itself
        _.bindAll(this, 'render');

        // Bind collection changes to re-rendering
        this.collection.bind('reset', this.render);
        this.collection.bind('add', this.render);
        this.collection.bind('remove', this.render);
    },
    template: _.template("<h3>Hello</h3><% _.each(collection, function(model) { %><%= model %><% }); %>"),

    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){


        //alert ("Collection: " + this.collection);
        var element = jQuery(this.el);
        // Clear potential old entries first
        element.empty();

        console.log("ProjectList");
        console.log(this.collection);

        // Go through the collection items
        this.collection.forEach(function(item) {
            //alert("here: " + item);

            var itemView = new views.ProjectList({
                model: item
            });
            console.log("ProjectListItem");
            console.log(item);

            // Render the View
            element.append(itemView.render().el);
        });

        //this.$el.html(this.template({
        //	who: "Fred",
        //  collection: this.collection
        //}));

        return this;
    }
});

