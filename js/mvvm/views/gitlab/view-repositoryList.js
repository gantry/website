// ============================================
// RepositoryList View
// ============================================

views.RepositoryList = Backbone.View.extend({

    tagName: 'tbody',

    initialize: function(options) {
        // Ensure our methods keep the `this` reference to the view itself
        _.bindAll(this, 'render');

        // If the model changes we need to re-render
        this.model.bind('change', this.render);

        // Not all views are self-rendering. This one is.
        this.render();
    },

    render: function() {

        // Check Inputs (Debug Info)
        console.log("Repositorylist render : ");
        console.log(this.model.get('success').success );
        console.log(this.model.get('repositories').repositories );

        // Clear
        jQuery(this.el).empty();
        jQuery("#repositoryList").empty();

        // Add Table Header
        jQuery("#repositoryList").append(jQuery(
            '<thead>'
            + '<tr>'
            + '<th class="tabblue">Name</th>'
//            + '<th class="tabblue">Id</th>'
//            + '<th class="tabblue">Mode</th>'
            + '<th class="tabblue">Path</th>'
            + '<th class="tabblue">Type</th>'
            + '</tr>'
            + '</thead>'
        ));


        // Attributes
        repositoryShell = this.model.get('repositories');
        repositoryList = repositoryShell['repositories'];
        console.log("RepositoryList");
        console.log(repositoryList);
        if(repositoryList){
            for (var i = 0; i < repositoryList.length; i++)
            {
                repository = repositoryList[i];
                console.log(repository);
                jQuery(this.el).append(jQuery(
                    '<tr class="serviceItem">'
                    + '<td>'+ repository["name"]  + '</td>'
//                    + '<td>'+ repository["id"]  + '</td>'
    //                + '<td>'+ repository["mode"].title  + '</td>'
                    + '<td>'+ repository["path"]  + '</td>'
                    + '<td>'+ repository["type"]  + '</td>'

                    + '</tr>'
                ));
            }
        }
        return this;
    }
});


views.RepositoryListView = Backbone.View.extend({

    // The collection will be kept here
    collection: null,
    el: '#repositoryList',

    initialize: function(options){
        this.collection = options.collection;

        // Ensure our methods keep the `this` reference to the view itself
        _.bindAll(this, 'render');

        // Bind collection changes to re-rendering
        this.collection.bind('reset', this.render);
        this.collection.bind('add', this.render);
        this.collection.bind('remove', this.render);
    },
    template: _.template("<h3>Hello</h3><% _.each(collection, function(model) { %><%= model %><% }); %>"),

    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){


        //alert ("Collection: " + this.collection);
        var element = jQuery(this.el);
        // Clear potential old entries first
        element.empty();

        console.log("RepositoryList");
        console.log(this.collection);

        // Go through the collection items
        this.collection.forEach(function(item) {
            //alert("here: " + item);

            var itemView = new views.RepositoryList({
                model: item
            });
            console.log("RepositoryListItem");
            console.log(item);

            // Render the View
            element.append(itemView.render().el);
        });

        //this.$el.html(this.template({
        //	who: "Fred",
        //  collection: this.collection
        //}));

        return this;
    }
});

