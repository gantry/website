// ============================================
// GroupList View
// ============================================

views.GroupList = Backbone.View.extend({

    tagName: 'div',

    initialize: function(options) {
        // Ensure our methods keep the `this` reference to the view itself
        _.bindAll(this, 'render');

        // If the model changes we need to re-render
        this.model.bind('change', this.render);

        // Not all views are self-rendering. This one is.
        this.render();
    },

    render: function() {

        // Check Inputs (Debug Info)
        console.log("GroupList render : ");

        // Clear
        jQuery(this.el).empty();

        groupList = this.model.get('groups').groups;
        console.log("GroupList");
        console.log(groupList);
        for (var i = 0; i < groupList.length; i++)
        {
            group = groupList[i];
            console.log(group);
            jQuery(this.el).append(jQuery(
                '<div class="serviceItem">'
                + '<hr/>'
                + '<b>'+ group["name"] + '</b>'
                + '<ul>'

                + '<li>'+ group["name"]  + '</li>'
                + '<li>'+ group["description"]  + '</li>'
                + '<li>'+ group["id"]  + '</li>'
                + '<li>'+ group["avatar_url"]  + '</li>'
                + '<li>'+ group["full_name"]  + '</li>'
                + '<li>'+ group["full_path"]  + '</li>'
//                + '<li>'+ group["ldap_access"]  + '</li>'
//                + '<li>'+ group["ldap_cn"]  + '</li>'
//                + '<li>'+ group["lfs_enabled"]  + '</li>'
//                + '<li>'+ group["parent_id"]  + '</li>'
                + '<li>'+ group["path"]  + '</li>'
                + '<li>'+ group["request_access_enabled"]  + '</li>'
                + '<li>'+ group["visibility"]  + '</li>'
                + '<li>'+ group["web_url"]  + '</li>'

                + '</ul>'
                + '</div>'
            ));
        }

        return this;
    }
});


views.GroupListView = Backbone.View.extend({

    // The collection will be kept here
    collection: null,
    el: '#groupList',

    initialize: function(options){
        this.collection = options.collection;

        // Ensure our methods keep the `this` reference to the view itself
        _.bindAll(this, 'render');

        // Bind collection changes to re-rendering
        this.collection.bind('reset', this.render);
        this.collection.bind('add', this.render);
        this.collection.bind('remove', this.render);

    },
    template: _.template("<h3>Hello</h3><% _.each(collection, function(model) { %><%= model %><% }); %>"),

    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){


        //alert ("Collection: " + this.collection);
        var element = jQuery(this.el);
        // Clear potential old entries first
        element.empty();

        console.log("GroupList");
        console.log(this.collection);

        // Go through the collection items
        this.collection.forEach(function(item) {
            //alert("here: " + item);

            var itemView = new views.GroupList({
                model: item
            });
            console.log("GroupListItem");
            console.log(item);

            // Render the View
            element.append(itemView.render().el);
        });

        //this.$el.html(this.template({
        //	who: "Fred",
        //  collection: this.collection
        //}));

        return this;
    }
});

