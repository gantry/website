//=======================================================================
// Router is responsible for driving the application. Usually
// this means populating the necessary data into models and
// collections, and then passing those to be displayed by
// appropriate views.
//=======================================================================


// ------------------------
// ProjectList
// ------------------------
function getProjectList(){
    console.log("Creating ProjectList Model")
    var projectListItems = new models.ProjectListItems({ host: projectListRoot });
    console.log(projectListItems)
    projectListItems.fetch();
    console.log(projectListItems)
    return projectListItems;
}
function renderProjectList(projectListItems) {
    console.log("Rendering ProjectList View")
    var projectListView = new views.ProjectListView({ collection : projectListItems});
}

// ------------------------
// CommitList
// ------------------------
function getCommitList(){
    console.log("Creating CommitList Model")
    var commitListItems = new models.CommitListItems({ host: commitListRoot });
    console.log(commitListItems)
    commitListItems.fetch();
    console.log(commitListItems)
    return commitListItems;
}
function renderCommitList(commitListItems) {
    console.log("Rendering CommitList View")
    var commitListView = new views.CommitListView({ collection : commitListItems});
}

// ------------------------
// IssueList
// ------------------------
function getIssueList(){
    console.log("Creating IssueList Model")
    var issueListItems = new models.IssueListItems({ host: issueListRoot });
    console.log(issueListItems)
    issueListItems.fetch();
    console.log(issueListItems)
    return issueListItems;
}
function renderIssueList(issueListItems) {
    console.log("Rendering IssueList View")
    var issueListView = new views.IssueListView({ collection : issueListItems});
}

// ------------------------
// MessageList
// ------------------------
function getMessageList(){
    console.log("Creating MessageList Model")
    var messageListItems = new models.MessageListItems({ host: messageListRoot });
    console.log(messageListItems)
    messageListItems.fetch();
    console.log(messageListItems)
    return messageListItems;
}
function renderMessageList(messageListItems) {
    console.log("Rendering MessageList View")
    var messageListView = new views.MessageListView({ collection : messageListItems});
}

// ------------------------
// GroupList
// ------------------------
function getGroupList(){
    console.log("Creating GroupList Model")
    var groupListItems = new models.GroupListItems({ host: groupListRoot });
    console.log(groupListItems)
    groupListItems.fetch();
    console.log(groupListItems)
    return groupListItems;
}
function renderGroupList(groupListItems) {
    console.log("Rendering GroupList View")
    var groupListView = new views.GroupListView({ collection : groupListItems});
}

// ------------------------
// RepositoryList
// ------------------------
function getRepositoryList(){
    console.log("Creating RepositoryList Model")
    var repositoryListItems = new models.RepositoryListItems({ host: repositoryListRoot });
    console.log(repositoryListItems)
    repositoryListItems.fetch();
    console.log(repositoryListItems)
    return repositoryListItems;
}
function renderRepositoryList(repositoryListItems) {
    console.log("Rendering RepositoryList View")
    var repositoryListView = new views.RepositoryListView({ collection : repositoryListItems});
}

// ------------------------
// LxdList
// ------------------------
function getLxdList(){
    console.log("Creating LxdList Model")
    var lxdListItems = new models.LxdListItems({ host: lxdListRoot });
    console.log(lxdListItems)
    lxdListItems.fetch();
    console.log(lxdListItems)
    return lxdListItems;
}
function renderLxdList(lxdListItems) {
    console.log("Rendering LxdList View")
    var lxdListView = new views.LxdListView({ collection : lxdListItems});
}


// ------------------------
// Controllers
// Create Views & Render
// ------------------------
function displayIndex() {

    console.log("======================================================= ProjectList =======================================================")
    var projectList = getProjectList();
    renderProjectList(projectList);

    console.log("======================================================= RepositoryList =======================================================")
    var repositoryList = getRepositoryList();
    renderRepositoryList(repositoryList);

    console.log("======================================================= IssueList =======================================================")
    var issueList = getIssueList();
    renderIssueList(issueList);

    console.log("======================================================= CommitList =======================================================")
    var commitList = getCommitList();
    renderCommitList(commitList);

    console.log("======================================================= GroupList =======================================================")
    var groupList = getGroupList();
    renderGroupList(groupList);

    console.log("======================================================= MessageList =======================================================")
    var messageList = getMessageList();
    renderMessageList(messageList);

    console.log("======================================================= Done =======================================================")

}

function displayLxd() {
    console.log("======================================================= LxdList =======================================================")
    var lxdList = getLxdList();
    renderLxdList(lxdList);
}




// ------------------------
// Main : Initialize Router & Events
// ------------------------
var AppRouter = Backbone.Router.extend({
  routes: {
        '': 'index',  // At first we display the index route
        'dashboard/services.html': 'services',
        'dashboard/project.html': 'services',
        'dashboard/compute.html': 'compute',
        'playlist/:id': 'getProjectList'
  },

  index: function() {
        console.log("Displaying Index")
        displayIndex();
        console.log("Finished");
  },
  services: function() {
        console.log("Displaying Services")
        displayIndex();
        console.log("Finished");
  },
  compute: function() {
        console.log("Displaying Compute")
        displayLxd();
        console.log("Finished");
  }
});


jQuery(document).ready(function() {

    console.log("Instantiating Router")
    // Instantiate the router
    var routerInstance = new AppRouter();

//    // Parameterized events
//    routerInstance.on('route:getCategory', function( id ){
//        //alert(id);
//        displayIndex(videoRoot + '?' + apiKey + paging + '&category[Genre]=' + id);
//    });
//    routerInstance.on('route:getProjectList', function( ){
//        console.log("Getting ProjectList")
//        displayIndex(projectListRoot);
//    });

    // Start routing
    //Backbone.history.start();
    Backbone.history.start({pushState: true});

    // Auto-Load Page
//      displayIndex();
//    routerInstance.navigate('dashboard/services.html', {trigger: true})
});



// Example of adding a new person after rendering
// This will fire the 'add' event in the collection which causes the view to re-render
//    people.add([{
//          firstname: 'Zaphod',
//          lastname: 'Beeblebrox'
//    }]);

