
//=======================================================================
// GroupList
//=======================================================================

models.GroupListItem = Backbone.Model.extend({
    defaults : {
//        _id: "",
        success: "false",
        groups: "",
    },
    initialize: function() {
        console.log("Initialized GroupListItem")
    },
    // Example of how to do a validation in a model
    validate: function(attributes) {
//        if (typeof attributes.success !== 'string') {
//            // Return a failed validation
//            return 'Success is mandatory';
//        }
//        // All validations passed, don't return anything
//        console.log("Validated")
    }

});

models.GroupListItems = Backbone.PageableCollection.extend({
    model: models.GroupListItem,
    url: groupListRoot,
    initialize: function(options) {
        this.url= options.host;
    },
    parse: function (data) {
        console.log("Response Data : ")
        console.log(data)
//        console.log("Response Data : ")
//        console.log(data.success)
        return data;
    },
    state: {pageSize: 100},
    mode: "client" // page entirely on the client side
});

//{
//    "groups": {
//        "groups": [
//            {
//                "avatar_url": null,
//                "description": "",
//                "full_name": "gantry",
//                "full_path": "gantry",
//                "id": 2,
//                "ldap_access": null,
//                "ldap_cn": null,
//                "lfs_enabled": true,
//                "name": "gantry",
//                "parent_id": null,
//                "path": "gantry",
//                "request_access_enabled": false,
//                "visibility": "public",
//                "web_url": "http://localhost/gitlab/groups/gantry"
//            }
//        ]
//    },
//    "success": {
//        "success": true
//    },
//    "test": "test message"
//}