
//=======================================================================
// CommitList
//=======================================================================

models.CommitListItem = Backbone.Model.extend({
    defaults : {
//        _id: "",
        success: "false",
        commits: "",
    },
    initialize: function() {
        console.log("Initialized CommitListItem")
    },
    // Example of how to do a validation in a model
    validate: function(attributes) {
//        if (typeof attributes.success !== 'string') {
//            // Return a failed validation
//            return 'Success is mandatory';
//        }
//        // All validations passed, don't return anything
//        console.log("Validated")
    }

});

models.CommitListItems = Backbone.PageableCollection.extend({
    model: models.CommitListItem,
    url: commitListRoot,
    initialize: function(options) {
        this.url= options.host;
    },
    parse: function (data) {
        console.log("Response Data : ")
        console.log(data)
        console.log("Response Data : ")
        console.log(data.success)
        return data;
    },
    state: {pageSize: 100},
    mode: "client" // page entirely on the client side
});

// Commits
//{
//    "author_email": "jason.s.hardman@gmail.com",
//    "author_name": "flatline",
//    "authored_date": "2018-08-27T15:20:34.000-04:00",
//    "committed_date": "2018-08-27T15:20:34.000-04:00",
//    "committer_email": "jason.s.hardman@gmail.com",
//    "committer_name": "flatline",
//    "created_at": "2018-08-27T15:20:34.000-04:00",
//    "id": "6b35bcb155e64051ace2a40675169362bcdd9963",
//    "message": "Initial Deploy\n",
//    "parent_ids": [
//        "679a09d5e22452a8b9f80d7a9ed12efbadc8b3af"
//    ],
//    "project_id": 1,
//    "short_id": "6b35bcb1",
//    "title": "Initial Deploy"
//},
