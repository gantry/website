
//=======================================================================
// Repositorylist
//=======================================================================

models.RepositoryListItem = Backbone.Model.extend({
    defaults : {
//        _id: "",
        success: "false",
        repositories: "",
    },
    initialize: function() {
        console.log("Initialized RepositoryListItem")
    },
    // Example of how to do a validation in a model
    validate: function(attributes) {
//        if (typeof attributes.success !== 'string') {
//            // Return a failed validation
//            return 'Success is mandatory';
//        }
//        // All validations passed, don't return anything
//        console.log("Validated")
    }

});

models.RepositoryListItems = Backbone.PageableCollection.extend({
    model: models.RepositoryListItem,
    url: repositoryListRoot,
    initialize: function(options) {
        this.url= options.host;
    },
    parse: function (data) {
        console.log("Response Data : ")
        console.log(data)
//        console.log("Response Data : ")
//        console.log(data.success)
        return data;
    },
    state: {pageSize: 100},
    mode: "client" // page entirely on the client side
});


//files": {
//    "files": [
//    {
//        "id": "232ca701d3b4b4bdbfcd3b2c3c2c9772eb296cb7",
//        "mode": "100644",
//        "name": ".gitlab-ci.yml",
//        "path": ".gitlab-ci.yml",
//        "type": "blob"
//    },
//    {
//        "id": "e69de29bb2d1d6434b8b29ae775ad8c2e48c5391",
//        "mode": "100644",
//        "name": "README.md",
//        "path": "README.md",
//        "type": "blob"
//    }
//    ]
//},