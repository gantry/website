
//=======================================================================
// MessageList
//=======================================================================

models.MessageListItem = Backbone.Model.extend({
    defaults : {
//        _id: "",
        success: "false",
        messages: "",
    },
    initialize: function() {
        console.log("Initialized MessageListItem")
    },
    // Example of how to do a validation in a model
    validate: function(attributes) {
//        if (typeof attributes.success !== 'string') {
//            // Return a failed validation
//            return 'Success is mandatory';
//        }
//        // All validations passed, don't return anything
//        console.log("Validated")
    }

});

models.MessageListItems = Backbone.PageableCollection.extend({
    model: models.MessageListItem,
    url: messageListRoot,
    initialize: function(options) {
        this.url= options.host;
    },
    parse: function (data) {
        console.log("Response Data : ")
        console.log(data)
//        console.log("Response Data : ")
//        console.log(data.success)
        return data;
    },
    state: {pageSize: 100},
    mode: "client" // page entirely on the client side
});

//{
//    "broadcastmessages": {
//        "messages": [
//            {
//                "active": false,
//                "color": "#E75E40",
//                "ends_at": "2018-10-03T22:57:29.342Z",
//                "font": "#FFFFFF",
//                "id": 215,
//                "message": "Important information soon",
//                "starts_at": "2018-10-03T21:57:29.342Z"
//            },
//            {
//                "active": false,
//                "color": "#E75E40",
//                "ends_at": "2018-10-03T22:43:25.898Z",
//                "font": "#FFFFFF",
//                "id": 214,
//                "message": "Important information soon",
//                "starts_at": "2018-10-03T21:43:25.898Z"
//            },
//        ]
//    },
//    "success": {
//        "success": true
//    },
//    "test": "test message"
//}
//
