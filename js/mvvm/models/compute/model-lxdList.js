
//=======================================================================
// LxdList
//=======================================================================

models.LxdListItem = Backbone.Model.extend({
    defaults : {
//        _id: "",
        success: "false",
        lxds: "",
    },
    initialize: function() {
        console.log("Initialized LxdListItem")
    },
    // Example of how to do a validation in a model
    validate: function(attributes) {
//        if (typeof attributes.success !== 'string') {
//            // Return a failed validation
//            return 'Success is mandatory';
//        }
//        // All validations passed, don't return anything
//        console.log("Validated")
    }

});

models.LxdListItems = Backbone.PageableCollection.extend({
    model: models.LxdListItem,
    url: lxdListRoot,
    initialize: function(options) {
        this.url= options.host;
    },
    parse: function (data) {
        console.log("Response Data : ")
        console.log(data)
        console.log("Response Data : ")
        console.log(data.success)
        return data;
    },
    state: {pageSize: 100},
    mode: "client" // page entirely on the client side
});

